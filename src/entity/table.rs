use rbatis::crud;
use rbatis::rbdc::datetime::DateTime;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct SysApiDb {
    pub api_id: Option<String>,
    pub db: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysDept {
    pub dept_id: String,
    pub parent_id: String,
    pub dept_name: String,
    pub order_num: i32,
    pub leader: Option<String>,
    pub phone: Option<String>,
    pub email: Option<String>,
    pub status: String,
    pub created_by: String,
    pub updated_by: Option<String>,
    pub created_at: DateTime,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}
crud!(SysDept{});

#[derive(Debug, Serialize, Deserialize)]
pub struct SysDictData {
    pub dict_data_id: String,
    pub dict_sort: i32,
    pub dict_label: String,
    pub dict_value: String,
    pub dict_type: String,
    pub css_class: Option<String>,
    pub list_class: Option<String>,
    pub is_default: String,
    pub status: String,
    pub create_by: String,
    pub update_by: Option<String>,
    pub remark: Option<String>,
    pub created_at: Option<DateTime>,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysDictType {
    pub dict_type_id: String,
    pub dict_name: String,
    pub dict_type: String,
    pub status: String,
    pub create_by: String,
    pub update_by: Option<String>,
    pub remark: Option<String>,
    pub created_at: Option<DateTime>,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysJobLog {
    pub job_log_id: String,
    pub job_id: String,
    pub lot_id: i64,
    pub lot_order: i64,
    pub job_name: String,
    pub job_group: String,
    pub invoke_target: String,
    pub job_params: Option<String>,
    pub job_message: Option<String>,
    pub status: String,
    pub exception_info: Option<String>,
    pub is_once: Option<String>,
    pub created_at: DateTime,
    pub elapsed_time: i64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysJob {
    pub job_id: String,
    pub task_id: i64,
    pub task_count: i64,
    pub run_count: i64,
    pub job_name: String,
    pub job_params: Option<String>,
    pub job_group: String,
    pub invoke_target: String,
    pub cron_expression: String,
    pub misfire_policy: String,
    pub concurrent: Option<String>,
    pub status: String,
    pub create_by: String,
    pub update_by: Option<String>,
    pub remark: Option<String>,
    pub last_time: Option<DateTime>,
    pub next_time: Option<DateTime>,
    pub end_time: Option<DateTime>,
    pub created_at: Option<DateTime>,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysLoginLog {
    pub info_id: String,
    pub login_name: String,
    pub net: String,
    pub ipaddr: String,
    pub login_location: String,
    pub browser: String,
    pub os: String,
    pub device: String,
    pub status: String,
    pub msg: String,
    pub login_time: DateTime,
    pub module: String,
}
crud!(SysLoginLog{});
#[derive(Debug, Serialize, Deserialize)]
pub struct SysMenu {
    pub id: String,
    pub pid: String,
    pub path: String,
    pub menu_name: String,
    pub icon: String,
    pub menu_type: String,
    pub query: Option<String>,
    pub order_sort: i32,
    pub status: String,
    pub api: String,
    pub method: String,
    pub component: String,
    pub visible: String,
    pub is_cache: String,
    pub log_method: String,
    pub data_cache_method: String,
    pub is_frame: String,
    pub data_scope: String,
    pub i18n: Option<String>,
    pub remark: String,
    pub created_at: Option<DateTime>,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysOperLog {
    pub oper_id: String,
    pub time_id: i64,
    pub title: String,
    pub business_type: String,
    pub method: String,
    pub request_method: String,
    pub operator_type: String,
    pub oper_name: String,
    pub dept_name: String,
    pub oper_url: String,
    pub oper_ip: String,
    pub oper_location: String,
    pub oper_param: String,
    pub path_param: String,
    pub json_result: String,
    pub status: String,
    pub error_msg: String,
    pub duration: i64,
    pub oper_time: DateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysPost {
    pub post_id: String,
    pub post_code: String,
    pub post_name: String,
    pub post_sort: i32,
    pub status: String,
    pub remark: Option<String>,
    pub created_by: String,
    pub updated_by: Option<String>,
    pub created_at: Option<DateTime>,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysRoleApi {
    pub id: String,
    pub role_id: String,
    pub api: String,
    pub method: Option<String>,
    pub created_by: String,
    pub created_at: DateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysRoleDept {
    pub role_id: String,
    pub dept_id: String,
    pub created_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysRole {
    pub role_id: String,
    pub role_name: String,
    pub role_key: String,
    pub list_order: i32,
    pub data_scope: String,
    pub status: String,
    pub remark: Option<String>,
    pub created_at: DateTime,
    pub updated_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysUpdateLog {
    pub id: String,
    pub app_version: String,
    pub backend_version: String,
    pub title: String,
    pub content: String,
    pub created_at: DateTime,
    pub updated_at: DateTime,
    pub deleted_at: Option<DateTime>,
    pub updated_by: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysUserDept {
    pub id: String,
    pub user_id: String,
    pub dept_id: String,
    pub created_by: String,
    pub created_at: DateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysUserOnline {
    pub id: String,
    pub u_id: String,
    pub token_id: String,
    pub token_exp: i64,
    pub login_time: DateTime,
    pub user_name: String,
    pub dept_name: String,
    pub net: String,
    pub ipaddr: String,
    pub login_location: String,
    pub device: String,
    pub browser: String,
    pub os: String,
}
crud!(SysUserOnline{});

#[derive(Debug, Serialize, Deserialize)]
pub struct SysUserPost {
    pub user_id: String,
    pub post_id: String,
    pub created_at: Option<DateTime>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysUserRole {
    pub id: String,
    pub user_id: String,
    pub role_id: String,
    pub created_by: String,
    pub created_at: DateTime,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SysUser {
    pub id: String,
    pub user_name: String,
    pub user_nickname: String,
    pub user_password: String,
    pub user_salt: String,
    pub user_status: String,
    pub user_email: Option<String>,
    pub sex: String,
    pub avatar: String,
    pub role_id: String,
    pub dept_id: String,
    pub remark: Option<String>,
    pub is_admin: String,
    pub phone_num: Option<String>,
    pub last_login_ip: Option<String>,
    pub last_login_time: Option<DateTime>,
    pub created_at: DateTime,
    pub updated_at: Option<DateTime>,
    pub deleted_at: Option<DateTime>,
}
crud!(SysUser{});
