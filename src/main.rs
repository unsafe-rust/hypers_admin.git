mod api;
mod config;
mod dto;
mod entity;
mod middleware;
mod service;
mod util;

use crate::{api::router, config::CFG};
use hypers::prelude::Result;

#[tokio::main]
async fn main() -> Result<()> {
    let root = router();
    let listener = hypers::TcpListener::bind(&CFG.server.address).await?;
    println!("Vist browser http://{}/swagger-ui/",&CFG.server.address);
    hypers::listen(root, listener).await
}
