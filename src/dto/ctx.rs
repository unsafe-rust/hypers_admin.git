use crate::middleware::jwt::JWTClaims;
#[derive(Clone, Debug)]
pub struct ReqCtx {
    pub ori_uri: String,
    pub path: String,
    pub path_params: String,
    pub method: String,
    pub user: JWTClaims,
    pub data: String,
}

