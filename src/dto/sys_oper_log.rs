use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct OperationLogData {
    pub req_id: String,
    pub req_user: String,
    pub req_time: String,
    pub time_id: i64,
    pub req_ip: String,
    pub req_version: String,
    pub req_method: String,
    pub req_url: String,
    pub req_ori_url: String,
    pub req_data: String,
    pub res_status: String,
    pub res_body: String,
    pub res_time: String,
    pub elapsed_time: String,
}
