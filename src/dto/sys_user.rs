use hypers::prelude::{Extract, ToSchema,hypers_openapi};
use rbatis::rbdc::DateTime;
use serde::{Deserialize, Serialize};
use super::sys_dept::DeptResp;

///  用户登录
#[derive(Deserialize, Debug, Extract,ToSchema)]
#[extract(body)]
pub struct UserLoginReq {
    ///  用户名
    pub user_name: String,
    ///  用户密码
    pub user_password: String,
    pub code: String,
    pub uuid: String,
}

#[derive(Debug, Serialize,Deserialize)]
pub struct UserResp {
    pub id: String,
    pub user_name: String,
    pub user_nickname: String,
    pub user_status: String,
    pub user_email: Option<String>,
    pub sex: String,
    pub avatar: String,
    pub dept_id: String,
    pub remark: Option<String>,
    pub is_admin: String,
    pub phone_num: Option<String>,
    pub role_id: String,
    pub created_at: Option<DateTime>,
}

#[derive(Debug, Serialize,Deserialize)]
pub struct UserWithDept {
    #[serde(flatten)]
    pub user: UserResp,
    pub dept: DeptResp,
}
