pub mod ctx;
pub mod sys_user;
/// 客户端信息以及返回信息
pub mod client;
pub mod sys_dept;
// 操作日志
pub mod sys_oper_log;

use serde::Serialize;
use hypers::prelude::{ToSchema,hypers_openapi};

#[derive(Debug, Serialize,ToSchema)]
pub struct CaptchaImage {
    pub captcha_on_off: bool,
    pub uuid: String,
    pub img: String,
}