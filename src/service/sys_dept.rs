use crate::dto::sys_dept::DeptResp;
use crate::config::DB;
use rbs::to_value;
use crate::entity::table::SysDept;
use crate::util::copy;

#[inline]
pub async fn get_by_id(id: &str) -> DeptResp {
    let dept = DB.query_decode::<SysDept>("SELECT * from sys_dept WHERE deleted_at IS NULL and dept_id = ?", vec![to_value!(id)]).await.expect("数据不存在");
    copy(&dept)
}