use crate::dto::client::ClientInfo;
use crate::entity::table::SysLoginLog;
use rbatis::rbdc::DateTime;
use crate::config::DB;

pub async fn add(req: ClientInfo, login_name: String, msg: String, status: String) {
    let info_id = scru128::new_string();
    let login_time = DateTime::now();
    let sys_login_log = SysLoginLog {
        info_id,
        login_name,
        net: req.net.net_work,
        ipaddr: req.net.ip,
        login_location: req.net.location,
        browser: req.ua.browser,
        os: req.ua.os,
        device: req.ua.device,
        status,
        msg,
        login_time,
        module: "系统后台".to_owned(),
    };
    let tx = DB.acquire_begin().await.expect("begin txn error");
    let mut tx = tx.defer_async(|mut tx| async move {
        if tx.done {
            return;
        } else {
            let _ = tx.rollback().await;
            return;
        }
    });
    SysLoginLog::insert(&tx, &sys_login_log).await.unwrap();
    let _ = tx.commit().await.expect("commit txn error");
}
