use crate::dto::CaptchaImage;
use crate::util::rand_utils::encrypt_password;
use math_captcha::Captcha;

/// 获取验证码
pub fn get_captcha() -> CaptchaImage {
    let captcha = Captcha::new(130, 40);
    let uuid = encrypt_password(&captcha.value.to_string(), &captcha.value.to_string());
    CaptchaImage {
        captcha_on_off: true,
        uuid,
        img: captcha.base64_img,
    }
}
