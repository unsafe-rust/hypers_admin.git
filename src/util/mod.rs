pub mod rand_utils;
pub mod web_utils;

use serde::{Serialize,de::DeserializeOwned};

#[inline]
pub fn copy<T>(source: &impl Serialize) -> T
where
    T: DeserializeOwned,
{
    serde_json::from_value::<T>(serde_json::to_value(source).unwrap()).unwrap()
}