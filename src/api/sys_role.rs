use hypers::prelude::*;

pub struct SysRole;

#[openapi(name = "role",tags((name = "角色", description = "角色模块")))]
impl SysRole {
    #[get(
        "/list",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_sort_list() -> impl Responder{}

    #[get(
        "/get_all",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_all() -> impl Responder{}

    #[get(
        "/get_by_id",
        tag = "按id获取",
        // responses(
        //     (status = 200, description = "按id获取", body = Data),
        // )
    )]
    async fn get_by_id() -> impl Responder{}

    #[post(
        "/add",
        tag = "添加",
        // responses(
        //     (status = 200, description = "添加", body = Data),
        // )
    )]
    async fn add() -> impl Responder{}

    #[put(
        "/edit",
        tag = "更新",
        // responses(
        //     (status = 200, description = "更新", body = Data),
        // )
    )]
    async fn edit() -> impl Responder{}

    #[put(
        "/update_auth_role",
        tag = "更新角色授权",
        // responses(
        //     (status = 200, description = "更新角色授权", body = Data),
        // )
    )]
    async fn update_auth_role() -> impl Responder{}

    #[put(
        "/change_status",
        tag = "设置状态",
        // responses(
        //     (status = 200, description = "设置状态", body = Data),
        // )
    )]
    async fn change_status() -> impl Responder{}

    #[put(
        "/set_data_scope",
        tag = "设置数据权限范围",
        // responses(
        //     (status = 200, description = "设置数据权限范围", body = Data),
        // )
    )]
    async fn set_data_scope() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}

    #[get(
        "/get_role_menu",
        tag = "获取角色菜单",
        // responses(
        //     (status = 200, description = "获取角色菜单", body = Data),
        // )
    )]
    async fn get_role_menu() -> impl Responder{}

    #[get(
        "/get_role_dept",
        tag = "获取角色部门",
        // responses(
        //     (status = 200, description = "获取角色部门", body = Data),
        // )
    )]
    async fn get_role_dept() -> impl Responder{}

    #[put(
        "/cancel_auth_user",
        tag = "批量用户取消角色授权",
        // responses(
        //     (status = 200, description = "批量用户取消角色授权", body = Data),
        // )
    )]
    async fn cancel_auth_user() -> impl Responder{}

    #[put(
        "/add_auth_user",
        tag = "批量用户角色授权",
        // responses(
        //     (status = 200, description = "批量用户角色授权", body = Data),
        // )
    )]
    async fn add_auth_user() -> impl Responder{}

    #[get(
        "/get_auth_users_by_role_id",
        tag = "获取角色对应用户",
        // responses(
        //     (status = 200, description = "获取角色对应用户", body = Data),
        // )
    )]
    async fn get_auth_users_by_role_id() -> impl Responder{}

    #[get(
        "/get_un_auth_users_by_role_id",
        tag = "获取角色对应未授权用户",
        // responses(
        //     (status = 200, description = "获取角色对应未授权用户", body = Data),
        // )
    )]
    async fn get_un_auth_users_by_role_id() -> impl Responder{}

}

