use hypers::prelude::*;

pub struct SysUser;

#[openapi(prefix = "system",name = "user",tags((name = "user", description = "用户管理")))]
impl SysUser {
    #[get(
        "list",
        tag = "获取全部用户,page_params 分页参数",
        // responses(
        //     (status = 200, description = "获取全部用户", body = Data),
        // )
    )] 
    async fn get_sort_list() -> impl Responder {}

    #[get(
        "get_by_id",
        tag = "按id获取用户",
        // responses(
        //     (status = 200, description = "按id获取用户", body = Data),
        // )
    )]
    async fn get_by_id() -> impl Responder {}

    #[get(
        "get_profile",
        tag = "按当前获取用户信息",
        // responses(
        //     (status = 200, description = "按当前获取用户信息", body = Data),
        // )
    )]
    async fn get_profile() -> impl Responder {}

    #[put(
        "update_profile",
        tag = "更新用户信息",
        // responses(
        //     (status = 200, description = "更新用户信息", body = Data),
        // )
    )]
    async fn update_profile() -> impl Responder {}

    #[post(
        "add",
        tag = "添加用户",
        // responses(
        //     (status = 200, description = "添加用户", body = Data),
        // )
    )]
    async fn add() -> impl Responder {}

    #[put(
        "edit",
        tag = "更新用户",
        // responses(
        //     (status = 200, description = "更新用户", body = Data),
        // )
    )]
    async fn edit() -> impl Responder {}

    #[delete(
        "delete",
        tag = "硬删除用户",
        // responses(
        //     (status = 200, description = "硬删除用户", body = Data),
        // )
    )]
    async fn delete() -> impl Responder {}

    #[get(
        "get_info",
        tag = "获取用户信息",
        // responses(
        //     (status = 200, description = "获取用户信息", body = Data),
        // )
    )]
    async fn get_info() -> impl Responder {}

    #[put(
        "reset_passwd",
        tag = "重置密码",
        // responses(
        //     (status = 200, description = "重置密码", body = Data),
        // )
    )]
    async fn reset_passwd() -> impl Responder {}

    #[put(
        "update_passwd",
        tag = "更新密码",
        // responses(
        //     (status = 200, description = "更新密码", body = Data),
        // )
    )]
    async fn update_passwd() -> impl Responder {}

    #[put(
        "change_status",
        tag = "修改状态",
        // responses(
        //     (status = 200, description = "修改状态", body = Data),
        // )
    )]
    async fn change_status() -> impl Responder {}

    #[put(
        "change_role",
        tag = "切换角色",
        // responses(
        //     (status = 200, description = "切换角色", body = Data),
        // )
    )]
    async fn change_role() -> impl Responder {}

    #[put(
        "change_dept",
        tag = "切换部门",
        // responses(
        //     (status = 200, description = "切换部门", body = Data),
        // )
    )]
    async fn change_dept() -> impl Responder {}

    #[put(
        "fresh_token",
        tag = "修改状态",
        // responses(
        //     (status = 200, description = "修改状态", body = Data),
        // )
    )]
    async fn fresh_token() -> impl Responder {}

    #[post(
        "update_avatar",
        tag = "修改头像",
        // responses(
        //     (status = 200, description = "修改头像", body = Data),
        // )
    )]
    async fn update_avatar() -> impl Responder {}
}
