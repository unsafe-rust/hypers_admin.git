use super::ApiResult;
use crate::dto::sys_user::UserLoginReq;
use crate::dto::CaptchaImage;
use crate::middleware::jwt::{get_current_user, AuthBody,jwt_auth_fn};
use hypers::prelude::*;
pub struct SysMonitor;

#[openapi(name = "monitor",tags((name = "服务器信息", description = "服务器信息模块")))]
impl SysMonitor {
    #[get(
        "/server",
        tag = "服务器信息"
        // responses(
        //     (status = 200, description = "服务器信息", body = Data),
        // )
    )]
    async fn get_server_info() -> impl Responder {}

    #[get(
        "/server-event",
        tag = "服务器信息"
        // responses(
        //     (status = 200, description = "服务器信息", body = Data),
        // )
    )]
    async fn get_server_info_ws() -> impl Responder {}
}

pub struct NoAuthApi;

#[openapi(
    name = "comm",
    tags((name = "通用模块", description = "无需授权Api.通用模块")),
    components(
        schemas(AuthBody,UserLoginReq,CaptchaImage)
    )
)]
impl NoAuthApi {
    #[post(
        "login",
        tag = "用户登录",
        request_body = UserLoginReq,
        responses(
            (status = 200, description = "登录", body = AuthBody),
        )
    )]
    async fn login(req: &mut Request, login_req: UserLoginReq) -> impl Responder {
        match crate::service::sys_user::login(login_req, req).await {
            Ok(v) => ApiResult::with_data(v),
            Err(e) => ApiResult::with_err(&e.to_string()),
        }
    }

    #[post(
        "log_out",
        tag = "退出登录",
        hook = [jwt_auth_fn],
        responses(
            (status = 200, description = "退出登录", body = String),
        )
    )]
    async fn log_out(req: &mut Request) -> impl Responder {
        let user = get_current_user(req)?;
        match crate::service::sys_user_online::log_out(&user.token_id).await {
            Ok(v) => Ok::<_, Error>(ApiResult::with_data(v)),
            Err(e) => Ok::<_, Error>(ApiResult::with_err(&e.to_string())),
        }
    }

    #[get(
        "get_captcha",
        tag = "获取验证码",
        responses(
            (status = 200, description = "获取验证码", body = CaptchaImage),
        )
    )]
    async fn get_captcha() -> impl Responder {
        ApiResult::with_data(crate::service::common::get_captcha())
    }
}
