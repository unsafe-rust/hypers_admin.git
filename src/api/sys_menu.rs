use hypers::prelude::*;

pub struct SysMenu;

#[openapi(name = "menu",tags((name = "menu", description = "路由菜单模块")))]
impl SysMenu {
    #[get(
        "/list",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_sort_list() -> impl Responder{}

    #[get(
        "/get_by_id",
        tag = "按id获取",
        // responses(
        //     (status = 200, description = "按id获取", body = Data),
        // )
    )]
    async fn get_by_id() -> impl Responder{}

    #[post(
        "/add",
        tag = "添加",
        // responses(
        //     (status = 200, description = "添加", body = Data),
        // )
    )]
    async fn add() -> impl Responder{}

    #[put(
        "/edit",
        tag = "更新",
        // responses(
        //     (status = 200, description = "更新", body = Data),
        // )
    )]
    async fn edit() -> impl Responder{}

    #[put(
        "/update_log_cache_method",
        tag = "更新api缓存方式和日志记录方式",
        // responses(
        //     (status = 200, description = "更新api缓存方式和日志记录方式", body = Data),
        // )
    )]
    async fn update_log_cache_method() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}

    #[get(
        "/get_all_enabled_menu_tree",
        tag = "获取全部正常的路由菜单树",
        // responses(
        //     (status = 200, description = "获取全部正常的路由菜单树", body = Data),
        // )
    )]
    async fn get_all_enabled_menu_tree() -> impl Responder{}

    #[get(
        "/get_routers",
        tag = "获取用户菜单树",
        // responses(
        //     (status = 200, description = "获取用户菜单树", body = Data),
        // )
    )]
    async fn get_routers() -> impl Responder{}

    #[get(
        "/get_auth_list",
        tag = "获取用户菜单树",
        // responses(
        //     (status = 200, description = "获取用户菜单树", body = Data),
        // )
    )]
    async fn get_related_api_and_db() -> impl Responder{}
}

