use hypers::prelude::*;

pub struct SysUpdateLog;

#[openapi(name = "update_log",tags((name = "更新日志", description = "更新日志模块")))]
impl SysUpdateLog {
    #[post(
        "/add",
        tag = "添加",
        // responses(
        //     (status = 200, description = "添加", body = Data),
        // )
    )]
    async fn add() -> impl Responder{}

    #[put(
        "/edit",
        tag = "更新",
        // responses(
        //     (status = 200, description = "更新", body = Data),
        // )
    )]
    async fn edit() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}

    #[get(
        "/get_all",
        tag = "获取全部",
        // responses(
        //     (status = 200, description = "获取全部", body = Data),
        // )
    )]
    async fn get_all() -> impl Responder{}
}

