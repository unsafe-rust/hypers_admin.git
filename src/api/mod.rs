mod common;
// mod sys_api_db;
mod sys_dept;
mod sys_dict_data;
mod sys_dict_type;
mod sys_job;
mod sys_job_log;
mod sys_login_log;
mod sys_menu;
mod sys_oper_log;
mod sys_post;
mod sys_role;
mod sys_update_log;
mod sys_user;
mod sys_user_online;

use common::{NoAuthApi, SysMonitor};
use hypers::prelude::{Router,Responder};
use serde::Serialize;
use sys_dept::SysDept;
use sys_dict_data::SysDictData;
use sys_dict_type::SysDictType;
use sys_job::SysJob;
use sys_job_log::SysJobLog;
use sys_login_log::SysLoginLog;
use sys_menu::SysMenu;
use sys_oper_log::SysOperLog;
use sys_post::SysPost;
use sys_role::SysRole;
use sys_update_log::SysUpdateLog;
use sys_user::SysUser;
use sys_user_online::SysUserOnline;
pub fn router() -> Router {
    let mut root = Router::default();
    root.push(SysUser)
        .push(SysDictType)
        .push(SysDictData)
        .push(SysPost)
        .push(SysDept)
        .push(SysRole)
        .push(SysMenu)
        .push(SysLoginLog)
        .push(SysUserOnline)
        .push(SysJob)
        .push(SysJobLog)
        .push(SysOperLog)
        .push(SysMonitor)
        .push(SysUpdateLog)
        .push(NoAuthApi);
    root.swagger("swagger-ui", None);
    root
}

/// 数据统一返回格式
#[derive(Debug, Serialize, Default)]
pub struct ApiResult<T> {
    pub code: i32,
    pub msg: String,
    pub data: Option<T>,
}

impl<T: Serialize> ApiResult<T> {
    pub fn with_data(data: T) -> Self {
        Self {
            code: 200,
            data: Some(data),
            msg: "success".to_owned(),
        }
    }
    pub fn with_err(err: &str) -> Self {
        Self {
            code: 500,
            data: None,
            msg: err.to_owned(),
        }
    }
    pub fn with_msg(msg: &str) -> Self {
        Self {
            code: 200,
            data: None,
            msg: msg.to_owned(),
        }
    }
}

impl<T: Serialize> Responder for ApiResult<T> 
{
    fn response(self, builder: hypers::prelude::Response) -> hypers::prelude::Response {
        builder.json(&self)
    }
}