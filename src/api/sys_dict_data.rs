use hypers::prelude::*;

pub struct SysDictData;

#[openapi(prefix = "dict", name = "data",tags((name = "字典数据", description = "字典数据模块")))]
impl SysDictData {

    #[get(
        "/list",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_sort_list() -> impl Responder{}

    #[get(
        "/get_all",
        tag = "获取筛选分页", 
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_all() -> impl Responder{}

    #[get(
        "/get_by_id",
        tag = "按id获取",
        // responses(
        //     (status = 200, description = "按id获取", body = Data),
        // )
    )]
    async fn get_by_id() -> impl Responder{}

    #[get(
        "/get_by_type",
        tag = "按id获取",
        // responses(
        //     (status = 200, description = "按id获取", body = Data),
        // )
    )]
    async fn get_by_type() -> impl Responder{}

    #[post(
        "/add",
        tag = "添加",
        // responses(
        //     (status = 200, description = "添加", body = Data),
        // )
    )]
    async fn add() -> impl Responder{}

    #[put(
        "/edit",
        tag = "更新",
        responses(
            (status = 200, description = "更新", body = Data),
        )
    )]
    async fn edit() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}
}