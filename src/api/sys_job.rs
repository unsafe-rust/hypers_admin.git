use hypers::prelude::*;

pub struct SysJob;

#[openapi(name = "job",tags((name = "定时任务", description = "定时任务模块")))]
impl SysJob {
    #[get(
        "/list",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_sort_list() -> impl Responder{}

    #[get(
        "/get_by_id",
        tag = "按id获取",
        // responses(
        //     (status = 200, description = "按id获取", body = Data),
        // )
    )]
    async fn get_by_id() -> impl Responder{}

    #[put(
        "/change_status",
        tag = "设置状态",
        // responses(
        //     (status = 200, description = "设置状态", body = Data),
        // )
    )]
    async fn change_status() -> impl Responder{}

    #[put(
        "/run_task_once",
        tag = "设置状态",
        // responses(
        //     (status = 200, description = "设置状态", body = Data),
        // )
    )]
    async fn run_task_once() -> impl Responder{}

    #[post(
        "/add",
        tag = "添加",
        // responses(
        //     (status = 200, description = "添加", body = Data),
        // )
    )]
    async fn add() -> impl Responder{}

    #[put(
        "/edit",
        tag = "更新",
        // responses(
        //     (status = 200, description = "更新", body = Data),
        // )
    )]
    async fn edit() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}

    #[post(
        "/validate_cron_str",
        tag = "验证cron_str",
        // responses(
        //     (status = 200, description = "验证cron_str", body = Data),
        // )
    )]
    async fn validate_cron_str() -> impl Responder{}
}

