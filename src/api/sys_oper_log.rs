use hypers::prelude::*;

pub struct SysOperLog;

#[openapi(name = "oper_log",tags((name = "操作日志", description = "操作日志模块")))]
impl SysOperLog {
    #[get(
        "/list",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_sort_list() -> impl Responder{}

    #[get(
        "/get_by_id",
        tag = "按id获取",
        // responses(
        //     (status = 200, description = "按id获取", body = Data),
        // )
    )]
    async fn get_by_id() -> impl Responder{}

    #[delete(
        "/clean",
        tag = "清空",
        // responses(
        //     (status = 200, description = "清空", body = Data),
        // )
    )]
    async fn clean() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}
}

