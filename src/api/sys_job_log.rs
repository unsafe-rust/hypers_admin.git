use hypers::prelude::*;

pub struct SysJobLog;

#[openapi(name = "job_log",tags((name = "定时任务日志", description = "定时任务日志模块")))]
impl SysJobLog {
    #[get(
        "/list",
        tag = "获取筛选分页",
        // responses(
        //     (status = 200, description = "获取筛选分页", body = Data),
        // )
    )]
    async fn get_sort_list() -> impl Responder{}

    #[delete(
        "/clean",
        tag = "清空",
        // responses(
        //     (status = 200, description = "清空", body = Data),
        // )
    )]
    async fn clean() -> impl Responder{}

    #[delete(
        "/delete",
        tag = "硬删除",
        // responses(
        //     (status = 200, description = "硬删除", body = Data),
        // )
    )]
    async fn delete() -> impl Responder{}
}

